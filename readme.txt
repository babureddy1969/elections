School Election Software

Schools have elections every year in which teachers prepare lists of candidates, print them on paper, distribute them to all voters in classes, collect the 
voted chits, count votes and announce results in a day. All these can be eliminated by using software. The software allows the following tasks.

1. Prepare a list of candidates participating in elections in various categories
2. Display the list of candidates participating in elections in various categories
3. Candidates can join or drop out before the elections and making these changes are simple on a computer screen
4. On the day of election, one laptop is used in one classroom
5. Students will login into the election software
6. Once the login is succesfull, the list of candidates in various categories will be displayed
7. One minute timer is placed on the screen when candidates list is shown to avoid wastage of time
8. One candidate can be selected in one category
9. After all the categories are selected, the vote is saved
10. When all the students have cast their vote, the results can be announced immediately by signing in as an administrator
11. The results can be printed and displayed on notice boards

Technology

PHP 5.6 and Apache Server 2.0 have been used to build the web application. 
Flat files are used to save students list, candidates list and voters list. 
Separate files are used each year. Web Application can be deployed on Linux 
or Windows Server with at least 4GB RAM, 20 GB Hard Disk and Dual Core Pentium
CPU.

Price 

Software including deployement  Rs 20,000
Annual Maintenance Contract     Rs 10,000 includes bug fixes and deployment issues


