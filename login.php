<?php
session_start();
$_SESSION["ERROR"] = '';
require("data.php");
//echo $_POST["studentid"];
$_SESSION["VOTERS_DATA"] = getData("votes2016.csv");
$_SESSION["VOTERS_COUNT"] = count($_SESSION["VOTERS_DATA"]);
//print_r ($_SESSION["STUDENTS_DATA"]);

if (isset($_SESSION["VOTERS_DATA"][$_POST["studentid"]])){
	$msg = "Sorry you have already cast your vote..." . $_POST["studentid"];
	$_SESSION["ERROR"] = $msg;
	header("Location: /index.php");
	die();
}

$pieces = explode(",",$_SESSION["STUDENTS_DATA"][$_POST["studentid"]]);
$_SESSION["voterid"] = $_POST["studentid"];
//echo $_POST["studentid"];
//print_r($_SESSION["STUDENTS_DATA"]);
//exit();
if (!isset($_SESSION["STUDENTS_DATA"][$_POST["studentid"]])) {
	$msg = "Invalid student id..." . $_POST["studentid"];
	$_SESSION["ERROR"] = $msg;
	header("Location: /index.php" );
	die();
}
header("Location: /candidates.php");
die();

?>
