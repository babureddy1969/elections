<?php
session_start();

function getData($csvFile){
	$file_handle = fopen($csvFile, 'r');
	$data = [];
	while (($line = fgets($file_handle)) !== false) {
		$pieces = explode(",", $line);
	    $data[$pieces[0]] = $line;
	}
	$_SESSION["STUDENTS_COUNT"] = count($_SESSION["STUDENTS_DATA"]);
	fclose($file_handle);
    return $data;
}
function getCategories($csvFile){
	$file_handle = fopen($csvFile, 'r');
	$data = [];
	$count=0;
	while (($line = fgets($file_handle)) !== false) {
	    $data[$count] = $line;
		$count = $count + 1;
	}
	fclose($file_handle);
    return $data;
}

if (!isset($_SESSION["STUDENTS_DATA"])) {	
	$_SESSION["STUDENTS_DATA"] = getData("students.csv");
	$_SESSION["STUDENTS_COUNT"] = count($_SESSION["STUDENTS_DATA"]);
}
if (!isset($_SESSION["VOTERS_DATA"])) {	
	$_SESSION["VOTERS_DATA"] = getData("votes2016.csv");
	$_SESSION["VOTERS_COUNT"] = count($_SESSION["VOTERS_DATA"]);
}
if (!isset($_SESSION["CANDIDATES_DATA"])) {	
	$_SESSION["CANDIDATES_DATA"] = getData("candidates2016.csv");
	$_SESSION["CANDIDATES_COUNT"] = count($_SESSION["CANDIDATES_DATA"]);
}

if (!isset($_SESSION["CATEGORIES"])) {	
	$_SESSION["CATEGORIES"] = getCategories("categories2016.csv");
	$_SESSION["CATEGORIES_COUNT"] = count($_SESSION["CATEGORIES"]);
}
//print_r ($_SESSION["STUDENTS_DATA"]);
//echo $_SESSION["STUDENTS_COUNT"];

//print_r ($_SESSION["VOTERS_DATA"]);
//echo $_SESSION["VOTERS_COUNT"];

//print_r ($_SESSION["CANDIDATES_DATA"]);
//echo $_SESSION["CANDIDATES_COUNT"];

print_r ($_SESSION["CATEGORIES"]);
echo $_SESSION["CATEGORIES_COUNT"];
